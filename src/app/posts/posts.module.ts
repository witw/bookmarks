import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { MatListModule } from '@angular/material/list';

import { PostListComponent } from './post-list/post-list.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    MatListModule
  ],
  declarations: [PostListComponent],
  exports: [PostListComponent]
})
export class PostsModule { }
